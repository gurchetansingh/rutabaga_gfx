// Copyright 2021 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

///! C-bindings for the rutabaga_gfx crate
extern crate rutabaga_gfx;

use std::mem;
use std::ptr::{null, null_mut};
use std::slice::{from_raw_parts, from_raw_parts_mut};

use std::ffi::CStr;
use std::path::PathBuf;

use std::os::raw::c_char;

use base::{FromRawDescriptor, IntoRawDescriptor, SafeDescriptor};
use data_model::VolatileSlice;
use rutabaga_gfx::*;

const NO_ERROR: i32 = 0;
const EINVAL: i32 = -22;

macro_rules! return_result {
    ($result:expr) => {
        if let Err(e) = $result {
            println!("Received an error {}", e);
            EINVAL
        } else {
            NO_ERROR
        }
    };
}

macro_rules! check_result {
    ($result:expr) => {
        match $result {
            Ok(t) => t,
            Err(e) => {
                println!("Received an error {}", e);
                return EINVAL;
            }
        }
    };
}

const RUTABAGA_COMPONENT_2D: u32 = 1;
const RUTABAGA_COMPONENT_VIRGL_RENDERER: u32 = 2;
const RUTABAGA_COMPONENT_GFXSTREAM: u32 = 3;
const RUTABAGA_COMPONENT_CROSS_DOMAIN: u32 = 4;

#[repr(C)]
pub struct rutabaga(());

#[allow(non_camel_case_types)]
type rutabaga_create_blob = ResourceCreateBlob;

#[allow(non_camel_case_types)]
type rutabaga_create_3d = ResourceCreate3D;

#[allow(non_camel_case_types)]
type rutabaga_transfer = Transfer3D;

#[allow(non_camel_case_types)]
type rutabaga_iovec = RutabagaIovec;

#[allow(non_camel_case_types)]
type rutabaga_fence = RutabagaFence;

#[repr(C)]
#[derive(Copy, Clone)]
pub struct rutabaga_iovecs {
    pub iovecs: *mut rutabaga_iovec,
    pub num_iovecs: usize,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct rutabaga_handle {
    pub os_handle: i32,
    pub handle_type: u32,
}

#[repr(C)]
pub struct rutabaga_channel {
    pub channel_name: *const c_char,
    pub channel_type: u32,
}

#[allow(non_camel_case_types)]
pub type write_fence_cb = extern "C" fn(user_data: u64, fence_data: rutabaga_fence);

#[repr(C)]
pub struct rutabaga_builder {
    pub user_data: u64,
    pub default_component: u32,
    pub fence_cb: write_fence_cb,
    pub channels: *mut rutabaga_channel,
    pub num_channels: usize,
}

fn rutabaga_from_ptr(ptr: *mut rutabaga) -> &'static mut Rutabaga {
    unsafe { &mut *(ptr as *mut Rutabaga) }
}

fn create_ffi_fence_handler(user_data: u64, fence_cb: write_fence_cb) -> RutabagaFenceHandler {
    RutabagaFenceClosure::new(move |completed_fence| fence_cb(user_data, completed_fence))
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_init(
    builder: *const rutabaga_builder,
    ptr: *mut *mut rutabaga,
) -> i32 {
    let fence_handler = create_ffi_fence_handler((*builder).user_data, (*builder).fence_cb);

    let component = match (*builder).default_component {
        RUTABAGA_COMPONENT_2D => RutabagaComponentType::Rutabaga2D,
        RUTABAGA_COMPONENT_VIRGL_RENDERER => RutabagaComponentType::VirglRenderer,
        RUTABAGA_COMPONENT_GFXSTREAM => RutabagaComponentType::Gfxstream,
        RUTABAGA_COMPONENT_CROSS_DOMAIN => RutabagaComponentType::CrossDomain,
        _ => {
            println!("unknown component type");
            return EINVAL;
        }
    };

    let virglrenderer_flags = VirglRendererFlags::new()
        .use_egl(true)
        .use_surfaceless(true)
        .use_external_blob(true);

    let mut rutabaga_channels: Vec<RutabagaChannel> = Vec::new();

    let gfxstream_flags = GfxstreamFlags::new()
        .use_egl(true)
        .use_surfaceless(true)
        .use_guest_angle(true)
        .use_syncfd(true)
        .use_vulkan(true);

    let channels = from_raw_parts((*builder).channels, (*builder).num_channels);
    for channel in channels {
        let result = CStr::from_ptr(channel.channel_name).to_str();
        let str_slice = check_result!(result);
        let string = str_slice.to_owned();
        let path = PathBuf::from(&string);

        rutabaga_channels.push(RutabagaChannel {
            base_channel: path,
            channel_type: channel.channel_type,
        });
    }

    let rutabaga_channels_opt = Some(rutabaga_channels);
    let result = RutabagaBuilder::new(component)
        .set_virglrenderer_flags(virglrenderer_flags)
        .set_gfxstream_flags(gfxstream_flags)
        .set_rutabaga_channels(rutabaga_channels_opt)
        .build(fence_handler);

    let rtbg = check_result!(result);
    *ptr = Box::into_raw(Box::new(rtbg)) as _;
    NO_ERROR
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_finish(ptr: *mut *mut rutabaga) -> i32 {
    let rtbg = *ptr as *mut Rutabaga;
    {
        Box::from_raw(rtbg);
    }
    *ptr = null_mut();
    NO_ERROR
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_get_num_capsets() -> u32 {
    let mut num_capsets = 0;

    // Cross-domain (like virtio_wl with llvmpipe) is always available.
    num_capsets += 1;

    // Three capsets for virgl_renderer
    #[cfg(feature = "virgl_renderer")]
    {
        num_capsets += 3;
    }

    // One capset for gfxstream
    #[cfg(feature = "gfxstream")]
    {
        num_capsets += 1;
    }

    num_capsets
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_get_capset_info(
    ptr: *mut rutabaga,
    capset_index: u32,
    capset_id: *mut u32,
    capset_version: *mut u32,
    capset_size: *mut u32,
) -> i32 {
    let result = rutabaga_from_ptr(ptr).get_capset_info(capset_index);
    let info = check_result!(result);
    *capset_id = info.0;
    *capset_version = info.1;
    *capset_size = info.2;
    NO_ERROR
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_get_capset(
    ptr: *mut rutabaga,
    capset_id: u32,
    version: u32,
    capset: *mut *mut u8,
) -> i32 {
    let result = rutabaga_from_ptr(ptr).get_capset(capset_id, version);
    let mut vec = check_result!(result);
    *capset = vec.as_mut_ptr();
    mem::forget(vec);
    NO_ERROR
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_context_create(
    ptr: *mut rutabaga,
    ctx_id: u32,
    context_init: u32,
) -> i32 {
    let result = rutabaga_from_ptr(ptr).create_context(ctx_id, context_init);
    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_context_destroy(ptr: *mut rutabaga, ctx_id: u32) -> i32 {
    let result = rutabaga_from_ptr(ptr).destroy_context(ctx_id);
    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_context_attach_resource(
    ptr: *mut rutabaga,
    ctx_id: u32,
    resource_id: u32,
) -> i32 {
    let result = rutabaga_from_ptr(ptr).context_attach_resource(ctx_id, resource_id);
    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_context_detach_resource(
    ptr: *mut rutabaga,
    ctx_id: u32,
    resource_id: u32,
) -> i32 {
    let result = rutabaga_from_ptr(ptr).context_detach_resource(ctx_id, resource_id);
    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_resource_create_3d(
    ptr: *mut rutabaga,
    resource_id: u32,
    create_3d: *const rutabaga_create_3d,
) -> i32 {
    let result = rutabaga_from_ptr(ptr).resource_create_3d(resource_id, *create_3d);
    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_resource_attach_backing(
    ptr: *mut rutabaga,
    resource_id: u32,
    iovecs: *const rutabaga_iovecs,
) -> i32 {
    let vecs: Vec<RutabagaIovec> =
        Vec::from_raw_parts((*iovecs).iovecs, (*iovecs).num_iovecs, (*iovecs).num_iovecs);

    let result = rutabaga_from_ptr(ptr).attach_backing(resource_id, vecs.clone());
    mem::forget(vecs);
    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_resource_detach_backing(
    ptr: *mut rutabaga,
    resource_id: u32,
) -> i32 {
    let result = rutabaga_from_ptr(ptr).detach_backing(resource_id);
    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_resource_transfer_read(
    ptr: *mut rutabaga,
    ctx_id: u32,
    resource_id: u32,
    transfer: *const rutabaga_transfer,
    buf: *mut u8,
    buf_size: usize,
) -> i32 {
    let mut slice_opt = None;
    if buf_size > 0 {
        slice_opt = Some(VolatileSlice::from_raw_parts(buf as *mut u8, buf_size));
    }

    let result = rutabaga_from_ptr(ptr).transfer_read(ctx_id, resource_id, *transfer, slice_opt);
    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_resource_transfer_write(
    ptr: *mut rutabaga,
    ctx_id: u32,
    resource_id: u32,
    transfer: *const rutabaga_transfer,
) -> i32 {
    let result = rutabaga_from_ptr(ptr).transfer_write(ctx_id, resource_id, *transfer);
    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_resource_create_blob(
    ptr: *mut rutabaga,
    ctx_id: u32,
    resource_id: u32,
    create_blob: *const rutabaga_create_blob,
    iovecs: *const rutabaga_iovecs,
    handle: *const rutabaga_handle,
) -> i32 {
    let mut iovecs_opt: Option<Vec<RutabagaIovec>> = None;
    if iovecs != null() {
        let vecs =
            Vec::from_raw_parts((*iovecs).iovecs, (*iovecs).num_iovecs, (*iovecs).num_iovecs);

        iovecs_opt = Some(vecs.clone());
        std::mem::forget(vecs);
    }

    let mut handle_opt: Option<RutabagaHandle> = None;
    if handle != null() {
        handle_opt = Some(RutabagaHandle {
            os_handle: SafeDescriptor::from_raw_descriptor((*handle).os_handle),
            handle_type: (*handle).handle_type,
        });
    }

    let result = rutabaga_from_ptr(ptr).resource_create_blob(
        ctx_id,
        resource_id,
        *create_blob,
        iovecs_opt,
        handle_opt,
    );

    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_resource_unref(ptr: *mut rutabaga, resource_id: u32) -> i32 {
    let result = rutabaga_from_ptr(ptr).unref_resource(resource_id);
    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_resource_export_blob(
    ptr: *mut rutabaga,
    resource_id: u32,
    handle: *mut rutabaga_handle,
) -> i32 {
    let result = rutabaga_from_ptr(ptr).export_blob(resource_id);
    let hnd = check_result!(result);

    (*handle).handle_type = hnd.handle_type;
    (*handle).os_handle = hnd.os_handle.into_raw_descriptor();
    NO_ERROR
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_resource_map_info(
    ptr: *mut rutabaga,
    resource_id: u32,
    map_info: *mut u32,
) -> i32 {
    let result = rutabaga_from_ptr(ptr).map_info(resource_id);
    *map_info = check_result!(result);
    NO_ERROR
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_submit_command(
    ptr: *mut rutabaga,
    ctx_id: u32,
    commands: *mut u8,
    size: usize,
) -> i32 {
    let cmd_slice = from_raw_parts_mut(commands, size);
    let result = rutabaga_from_ptr(ptr).submit_command(ctx_id, cmd_slice);
    return_result!(result)
}

#[no_mangle]
pub unsafe extern "C" fn rutabaga_create_fence(
    ptr: *mut rutabaga,
    fence: *const rutabaga_fence,
) -> i32 {
    let result = rutabaga_from_ptr(ptr).create_fence(*fence);
    return_result!(result)
}
