/*
 * Copyright 2021 The Chromium OS Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#ifndef RUTABAGA_GFX_FFI_H
#define RUTABAGA_GFX_FFI_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Rutabaga component types
 */
#define RUTABAGA_COMPONENT_2D 1
#define RUTABAGA_COMPONENT_VIRGL_RENDERER 2
#define RUTABAGA_COMPONENT_GFXSTREAM 3
#define RUTABAGA_COMPONENT_CROSS_DOMAIN 4

/**
 * Blob resource creation parameters.
 */
#define RUTABAGA_BLOB_MEM_GUEST 1
#define RUTABAGA_BLOB_MEM_HOST3D 2
#define RUTABAGA_BLOB_MEM_HOST3D_GUEST 3

#define RUTABAGA_BLOB_FLAG_USE_MAPPABLE 1
#define RUTABAGA_BLOB_FLAG_USE_SHAREABLE 2
#define RUTABAGA_BLOB_FLAG_USE_CROSS_DEVICE 4

/**
 * Rutabaga capsets.
 */
#define RUTABAGA_CAPSET_VIRGL 1
#define RUTABAGA_CAPSET_VIRGL2 2
#define RUTABAGA_CAPSET_GFXSTREAM 3
#define RUTABAGA_CAPSET_VENUS 4
#define RUTABAGA_CAPSET_CROSS_DOMAIN 5

/**
 * Mapped memory caching flags (see virtio_gpu spec)
 */
#define RUTABAGA_MAP_CACHE_CACHED 1
#define RUTABAGA_MAP_CACHE_UNCACHED 2
#define RUTABAGA_MAP_CACHE_WC 3

/**
 * Rutabaga flags for creating fences.
 */
#define RUTABAGA_FLAG_FENCE (1 << 0)
#define RUTABAGA_FLAG_INFO_RING_IDX (1 << 1)

/**
 * Rutabaga channel types
 */
#define RUTABAGA_CHANNEL_TYPE_WAYLAND 1
#define RUTABAGA_CHANNEL_TYPE_CAMERA 2

/**
 * Rutabaga handle types
 */
#define RUTABAGA_MEM_HANDLE_TYPE_OPAQUE_FD 1
#define RUTABAGA_MEM_HANDLE_TYPE_DMABUF 2
#define RUTABAGE_MEM_HANDLE_TYPE_OPAQUE_WIN32 3
#define RUTABAGA_FENCE_HANDLE_TYPE_OPAQUE_FD 4
#define RUTABAGA_FENCE_HANDLE_TYPE_SYNC_FD 5
#define RUTABAGE_FENCE_HANDLE_TYPE_OPAQUE_WIN32 6

struct rutabaga;

struct rutabaga_fence {
	uint32_t flags;
	uint64_t fence_id;
	uint32_t ctx_id;
	uint32_t ring_idx;
};

struct rutabaga_create_blob {
	uint32_t blob_mem;
	uint32_t blob_flags;
	uint64_t blob_id;
	uint64_t size;
};

struct rutabaga_create_3d {
	uint32_t target;
	uint32_t format;
	uint32_t bind;
	uint32_t width;
	uint32_t height;
	uint32_t depth;
	uint32_t array_size;
	uint32_t last_level;
	uint32_t nr_samples;
	uint32_t flags;
};

struct rutabaga_transfer {
	uint32_t x;
	uint32_t y;
	uint32_t z;
	uint32_t w;
	uint32_t h;
	uint32_t d;
	uint32_t level;
	uint32_t stride;
	uint32_t layer_stride;
	uint64_t offset;
};

struct rutabaga_iovec {
	void *base;
	size_t len;
};

struct rutabaga_iovecs {
	struct rutabaga_iovec *iovecs;
	size_t num_iovecs;
};

struct rutabaga_handle {
	int32_t os_handle;
	uint32_t handle_type;
};

/**
 * Assumes null-terminated C-string.
 */
struct rutabaga_channel {
	const char *channel_name;
	uint32_t channel_type;
};

typedef void (*write_fence_cb)(uint64_t user_data, struct rutabaga_fence fence_data);

struct rutabaga_builder {
	uint64_t user_data;
	uint32_t rutabaga_component;
	write_fence_cb fence_cb;
	struct rutabaga_channel *channels;
	size_t num_channels;
};

int32_t rutabaga_init(const struct rutabaga_builder *builder, struct rutabaga **ptr);

int32_t rutabaga_finish(struct rutabaga **ptr);

uint32_t rutabaga_get_num_capsets(void);

int32_t rutabaga_get_capset_info(struct rutabaga *ptr, uint32_t capset_index, uint32_t *capset_id,
				 uint32_t *capset_version, uint32_t *capset_size);

int32_t rutabaga_get_capset(struct rutabaga *ptr, uint32_t capset_id, uint32_t version,
			    uint8_t **capset);

int32_t rutabaga_context_create(struct rutabaga *ptr, uint32_t ctx_id, uint32_t context_init);

int32_t rutabaga_context_destroy(struct rutabaga *ptr, uint32_t ctx_id);

int32_t rutabaga_context_attach_resource(struct rutabaga *ptr, uint32_t ctx_id,
					 uint32_t resource_id);

int32_t rutabaga_context_detach_resource(struct rutabaga *ptr, uint32_t ctx_id,
					 uint32_t resource_id);

int32_t rutabaga_resource_create_3d(struct rutabaga *ptr, uint32_t resource_id,
				    const struct rutabaga_create_3d *create_3d);

int32_t rutabaga_resource_attach_backing(struct rutabaga *ptr, uint32_t resource_id,
					 const struct rutabaga_iovecs *iovecs);

int32_t rutabaga_resource_detach_backing(struct rutabaga *ptr, uint32_t resource_id);

int32_t rutabaga_resource_transfer_read(struct rutabaga *ptr, uint32_t ctx_id, uint32_t resource_id,
					const struct rutabaga_transfer *transfer, uint8_t *buf,
					size_t buf_size);

int32_t rutabaga_resource_transfer_write(struct rutabaga *ptr, uint32_t ctx_id,
					 uint32_t resource_id,
					 const struct rutabaga_transfer *transfer);

int32_t rutabaga_resource_create_blob(struct rutabaga *ptr, uint32_t ctx_id, uint32_t resource_id,
				      const struct rutabaga_create_blob *rutabaga_create_blob,
				      const struct rutabaga_iovecs *iovecs,
				      const struct rutabaga_handle *handle);

int32_t rutabaga_resource_unref(struct rutabaga *ptr, uint32_t resource_id);

int32_t rutabaga_resource_export_blob(struct rutabaga *ptr, uint32_t resource_id,
				      struct rutabaga_handle *handle);

int32_t rutabaga_resource_map_info(struct rutabaga *ptr, uint32_t resource_id, uint32_t *map_info);

int32_t rutabaga_submit_command(struct rutabaga *ptr, uint32_t ctx_id, uint8_t *commands,
				size_t size);

int32_t rutabaga_create_fence(struct rutabaga *ptr, const struct rutabaga_fence *fence);

#ifdef __cplusplus
}
#endif

#endif
